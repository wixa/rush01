/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   CPUModule.cpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kshyshki <kshyshki@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/14 10:18:00 by kshyshki          #+#    #+#             */
/*   Updated: 2018/04/14 10:18:00 by kshyshki         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "CPUModule.hpp"

void 	CPUModule::dell_data()
{
	_data.clear();
}

CPUModule::CPUModule()
{
	this->fillData();
}

CPUModule::CPUModule(CPUModule const &src)
{
	*this = src;
}

CPUModule::~CPUModule()
{
	this->dell_data();
}

CPUModule &CPUModule::operator=(CPUModule const &src)
{
	if (this != &src)
		_data = src._data;
	return *this;
}

void				CPUModule::fillData()
{
	int tmp;
	char tmp1[100];
	size_t	len = 100;
	sysctlbyname("machdep.cpu.brand_string", tmp1, &len, NULL, 0);
	_data["1.CPU Brand Name : "] = std::string(tmp1);
	sysctlbyname("machdep.cpu.core_count", &tmp, &len, NULL, 0);
	_data["2.CPU N Cores : "] = std::string(std::to_string(tmp));
	std::stringstream ss;
	char buff[128];
	FILE*    ret = popen("top -l 1 | head -n 10 | grep CPU", "r");
	while (!feof(ret))
	{
		if (fgets(buff, 128, ret))
			ss << buff;
	}
	pclose(ret);
	std::string tmp_str = ss.str();
	size_t start = tmp_str.rfind(':', std::string::npos);
	size_t end = tmp_str.find_first_of(' ' , start + 2);
	_data["3.CPU User usage : "] = std::string(tmp_str.substr(start + 2, end - start - 2));
	start = tmp_str.find(',', 0);
	end = tmp_str.find_first_of(' ' , start + 2);
	_data["4.CPU System usage : "] = std::string(tmp_str.substr(start + 2, end - start - 2));
	start = tmp_str.find_last_of(',', std::string::npos);
	end = tmp_str.find_first_of(' ' , start + 2);
	_data["5.CPU Idle usage : "] = std::string(tmp_str.substr(start + 2, end - start - 2));
}

std::map<std::string, std::string> *	CPUModule::get_data()
{
	return &_data;
};