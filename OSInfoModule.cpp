/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   OSInfoModule.cpp                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kshyshki <kshyshki@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/14 10:05:00 by kshyshki          #+#    #+#             */
/*   Updated: 2018/04/14 10:05:00 by kshyshki         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "OSInfoModule.hpp"

void 	OSInfoModule::dell_data()
{
	_data.erase("OS Name : ");
	_data.erase("OS Type : ");
	_data.erase("OS Release : ");
}

OSInfoModule::OSInfoModule()
{
	this->fillData();
}

OSInfoModule::OSInfoModule(OSInfoModule const &src)
{
	*this = src;
}

OSInfoModule::~OSInfoModule()
{
	this->dell_data();
}

OSInfoModule &OSInfoModule::operator=(OSInfoModule const &src)
{
	if (this != &src)
		_data = src._data;
	return *this;
}

void				OSInfoModule::fillData()
{
	this->dell_data();
	char tmp[64];
	char tmp1[64];
	char tmp2[64];
	size_t	len = 64;
	const char*     cmd ="sw_vers -productName";
	FILE*           pipe = popen(cmd, "r");

	if (!pipe)
		return ;
	while(!feof(pipe))
		fgets(tmp, 64, pipe);
	pclose(pipe);
	std::string name = std::string(tmp);
	name.erase(name.end() - 1);
	_data.insert(std::pair<std::string, std::string>("1.OS Name : ", name));
	sysctlbyname("kern.ostype", &tmp1, &len, NULL, 0);
	_data.insert(std::pair<std::string, std::string>("2.OS Type : ", std::string(tmp1)));
	sysctlbyname("kern.osrelease", &tmp2, &len, NULL, 0);
	_data.insert(std::pair<std::string, std::string>("3.OS Release : ", std::string(tmp2)));
}

std::map<std::string, std::string> *	OSInfoModule::get_data()
{
	return &_data;
};