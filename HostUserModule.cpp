/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   HostUserModule.cpp                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kshyshki <kshyshki@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/14 09:59:00 by kshyshki          #+#    #+#             */
/*   Updated: 2018/04/14 09:59:00 by kshyshki         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "HostUserModule.hpp"

void 	HostUserModule::dell_data()
{
	_data.clear();
}

HostUserModule::HostUserModule()
{
	this->fillData();
}

HostUserModule::HostUserModule(HostUserModule const &src)
{
	*this = src;
}

HostUserModule::~HostUserModule()
{
	this->dell_data();
}

HostUserModule &HostUserModule::operator=(HostUserModule const &src)
{
	if (this != &src)
		_data = src._data;
	return *this;
}

void				HostUserModule::fillData()
{
	this->dell_data();
	char tmp[64];
	size_t	len = 64;
	sysctlbyname("kern.hostname", &tmp, &len, NULL, 0);
	_data["1.Host Name : "] =  std::string(tmp);
	_data["2.User Name : "] = std::string(getlogin());
}

std::map<std::string, std::string> *	HostUserModule::get_data()
{
	return &_data;
};