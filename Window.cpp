/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Window.cpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kshyshki <kshyshki@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/15 13:47:00 by kshyshki          #+#    #+#             */
/*   Updated: 2018/04/15 13:47:00 by kshyshki         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Window.hpp"

Window::Window(QWidget *parent)
		: QWidget(parent)
{

}

Window::Window(std::vector<IMonitorModule*> & src, QWidget *parent)
		: QWidget(parent)
{
	_modules = src;
	__timer = new QTimer();
	__grid = new QGridLayout;
	__grid->addWidget(createHostNameInfo(), 0, 0);
	__grid->addWidget(createOsInfo(), 1, 0);
	__grid->addWidget(createDateTimeInfo(), 2, 0);
	__grid->addWidget(createCpuInfo(), 0, 1);
	__grid->addWidget(createRamInfo(), 1, 1);
	__grid->addWidget(createNetworkInfo(), 2, 1);
	resize(480, 320);
	setLayout(__grid);

	setWindowTitle(tr("SYSTEM MONITOR"));
	connect(this->__timer, SIGNAL(timeout()), this, SLOT(qt_refresh()));
	__timer->start(1000);
}

Window::Window()
{
}

Window::Window(Window const &src)
{
	*this = src;
}

Window::~Window()
{
}

Window &Window::operator=(Window const &src)
{
	if (this != &src)
	{
		_modules = src._modules;
	}
	return *this;
}

QGroupBox *Window::createHostNameInfo()
{
	QGroupBox	*groupBox = new QGroupBox(tr("HOST/NAME INFO"));

	std::pair<std::string, std::string> pair = *(_modules[0]->get_data()->find("1.Host Name : "));
	QLabel      *Label_hostName = new QLabel(QString(pair.first.c_str()) + QString(pair.second.c_str()));

	pair = *(_modules[0]->get_data()->find("2.User Name : "));
	QLabel      *Label_userName = new QLabel(QString(pair.first.c_str()) + QString(pair.second.c_str()));

	QVBoxLayout *vbox = new QVBoxLayout;
	vbox->addWidget(Label_hostName,1, Qt::AlignLeft );
	vbox->addWidget(Label_userName,1, Qt::AlignLeft );
	groupBox->setLayout(vbox);

	return groupBox;
}

QGroupBox *Window::createOsInfo()
{
	QGroupBox	*groupBox = new QGroupBox(tr("OS INFO"));

	std::map<std::string, std::string>::const_iterator pair = _modules[1]->get_data()->find("1.OS Name : ");
	QLabel      *Label_osName = new QLabel(QString(pair->first.c_str()) + QString(pair->second.c_str()));

	pair = _modules[1]->get_data()->find("2.OS Type : ");
	QLabel      *Label_osType = new QLabel(QString(pair->first.c_str()) + QString(pair->second.c_str()));

	pair = _modules[1]->get_data()->find("3.OS Release : ");
	QLabel      *Label_osRelease = new QLabel(QString(pair->first.c_str()) + QString(pair->second.c_str()));

	QVBoxLayout *vbox = new QVBoxLayout;
	vbox->addWidget(Label_osName,1, Qt::AlignLeft );
	vbox->addWidget(Label_osType,1, Qt::AlignLeft );
	vbox->addWidget(Label_osRelease,1, Qt::AlignLeft );
	groupBox->setLayout(vbox);

	return groupBox;
}

QGroupBox *Window::createDateTimeInfo()
{
	QGroupBox	*groupBox = new QGroupBox(tr("DATE/TIME INFO"));

	std::pair<std::string, std::string> pair = *(_modules[2]->get_data()->find("1.Date : "));
	QLabel      *Label_date = new QLabel(QString(pair.first.c_str()) + QString(pair.second.c_str()));

	pair = *(_modules[2]->get_data()->find("2.Time : "));
	QLabel      *Label_name = new QLabel(QString(pair.first.c_str()) + QString(pair.second.c_str()));

	QVBoxLayout *vbox = new QVBoxLayout;
	vbox->addWidget(Label_date,1, Qt::AlignLeft );
	vbox->addWidget(Label_name,1, Qt::AlignLeft );
	groupBox->setLayout(vbox);

	return groupBox;
}

QGroupBox *Window::createCpuInfo()
{
	QGroupBox	*groupBox = new QGroupBox(tr("CPU INFO"));

	std::pair<std::string, std::string> pair = *(_modules[3]->get_data()->find("1.CPU Brand Name : "));
	QLabel      *Label_cpuName = new QLabel(QString(pair.first.c_str()) + QString(pair.second.c_str()));

	pair = *(_modules[3]->get_data()->find("2.CPU N Cores : "));
	QLabel      *Label_cpuCores = new QLabel(QString(pair.first.c_str()) + QString(pair.second.c_str()));

	pair = *(_modules[3]->get_data()->find("3.CPU User usage : "));
	QLabel      *Label_cpuUser = new QLabel(QString(pair.first.c_str()) + QString(pair.second.c_str()));

	pair = *(_modules[3]->get_data()->find("4.CPU System usage : "));
	QLabel      *Label_cpuSys = new QLabel(QString(pair.first.c_str()) + QString(pair.second.c_str()));

	pair = *(_modules[3]->get_data()->find("5.CPU Idle usage : "));
	QLabel      *Label_cpuIdle = new QLabel(QString(pair.first.c_str()) + QString(pair.second.c_str()));

	QVBoxLayout *vbox = new QVBoxLayout;
	vbox->addWidget(Label_cpuName,1, Qt::AlignLeft );
	vbox->addWidget(Label_cpuCores,1, Qt::AlignLeft );
	vbox->addWidget(Label_cpuUser,1, Qt::AlignLeft );
	vbox->addWidget(Label_cpuSys,1, Qt::AlignLeft );
	vbox->addWidget(Label_cpuIdle,1, Qt::AlignLeft);
	groupBox->setLayout(vbox);

	return groupBox;
}

QGroupBox *Window::createRamInfo()
{
	QGroupBox	*groupBox = new QGroupBox(tr("RAM INFO"));

	std::map<std::string, std::string>::iterator pair = _modules[4]->get_data()->find("1.Ram used : ");
	QLabel      *Label_used = new QLabel(QString(pair->first.c_str()) + QString(pair->second.c_str()));

	pair = _modules[4]->get_data()->find("2.Ram free : ");
	QLabel      *Label_free = new QLabel(QString(pair->first.c_str()) + QString(pair->second.c_str()));

	QVBoxLayout *vbox = new QVBoxLayout;
	vbox->addWidget(Label_used,1, Qt::AlignLeft);
	vbox->addWidget(Label_free,1, Qt::AlignLeft);
	groupBox->setLayout(vbox);

	return groupBox;
}

QGroupBox *Window::createNetworkInfo()
{
	QGroupBox	*groupBox = new QGroupBox(tr("NETWORK INFO"));

	std::pair<std::string, std::string> pair = *(_modules[5]->get_data()->find("1.Input packets/memory : "));
	QLabel      *Label_input = new QLabel(QString(pair.first.c_str()) + QString(pair.second.c_str()));

	pair = *(_modules[5]->get_data()->find("2.Output packets/memory : "));
	QLabel      *Label_output = new QLabel(QString(pair.first.c_str()) + QString(pair.second.c_str()));

	QVBoxLayout *vbox = new QVBoxLayout;
	vbox->addWidget(Label_input,1, Qt::AlignLeft);
	vbox->addWidget(Label_output,1, Qt::AlignLeft );
	groupBox->setLayout(vbox);

	return groupBox;
}

void            Window::qt_refresh()
{
	QLayoutItem *tmp_layaout;
	while ((tmp_layaout = __grid->takeAt(0)))
	{
		delete tmp_layaout->widget();
		delete tmp_layaout;
	}
	for(int i = 0; i < 6; i++)
		_modules.at(i)->fillData();

	__grid->addWidget(createHostNameInfo(), 0, 0);
	__grid->addWidget(createOsInfo(), 1, 0);
	__grid->addWidget(createDateTimeInfo(), 2, 0);
	__grid->addWidget(createCpuInfo(), 0, 1);
	__grid->addWidget(createRamInfo(), 1, 1);
	__grid->addWidget(createNetworkInfo(), 2, 1);

	setLayout(__grid);
	update();
}
