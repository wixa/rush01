/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ADisplay.hpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kshyshki <kshyshki@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/14 15:32:00 by kshyshki          #+#    #+#             */
/*   Updated: 2018/04/14 15:32:00 by kshyshki         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */


#ifndef ADISPLAY_HPP
# define ADISPLAY_HPP

# include <iostream>
# include <vector>
# include "IMonitorDisplay.hpp"
# include "IMonitorModule.hpp"
# include "HostUserModule.hpp"
# include "OSInfoModule.hpp"
# include "DateTimeModule.hpp"
# include "CPUModule.hpp"
# include "RamModule.hpp"
# include "NetworkModule.hpp"

class ADisplay : public IMonitorDisplay
{
protected:
	std::vector<IMonitorModule*> _modules;
public:
	ADisplay();
	ADisplay(ADisplay const &src);
	~ADisplay();

	ADisplay &	operator=(ADisplay const &src);

	virtual void 			fillAllData();
	std::vector<IMonitorModule*>  getModules();
};

#endif
