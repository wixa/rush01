/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Window.hpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kshyshki <kshyshki@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/15 13:47:00 by kshyshki          #+#    #+#             */
/*   Updated: 2018/04/15 13:47:00 by kshyshki         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */


#ifndef WINDOW_HPP
# define WINDOW_HPP

#include <QWidget>
#include <QtWidgets>
#include <QGridLayout>
#include <QTimer>
#include <QObject>

#include "IMonitorModule.hpp"

QT_BEGIN_NAMESPACE
class QGroupBox;
QT_END_NAMESPACE

class Window : public QWidget
{
private:
	Q_OBJECT
	std::vector<IMonitorModule *> 	_modules;
	QTimer  *						__timer;
	QGridLayout * 					__grid;
	Window();
public:
	Window(QWidget *parent = 0);
	Window( std::vector<IMonitorModule *> & src, QWidget *parent = 0);
	Window(Window const &src);
	~Window();

	Window &operator=(Window const &src);

	QGroupBox *	createHostNameInfo();
	QGroupBox *	createOsInfo();
	QGroupBox *	createDateTimeInfo();
	QGroupBox *	createCpuInfo();
	QGroupBox *	createRamInfo();
	QGroupBox *	createNetworkInfo();

private slots:
	void		qt_refresh();

};
#endif
