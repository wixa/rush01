/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   IMonitorDisplay.hpp                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kshyshki <kshyshki@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/14 08:47:00 by kshyshki          #+#    #+#             */
/*   Updated: 2018/04/14 08:47:00 by kshyshki         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */


#ifndef IMONITORDISPLAY_HPP
# define IMONITORDISPLAY_HPP


class IMonitorDisplay
{
public:
	virtual ~IMonitorDisplay(){};
//	virtual void display() = 0;

	virtual void start() = 0;
	virtual void fillAllData() = 0;
};

#endif
