/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   NetworkModule.hpp                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kshyshki <kshyshki@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/14 11:37:00 by kshyshki          #+#    #+#             */
/*   Updated: 2018/04/14 11:37:00 by kshyshki         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */


#ifndef NETWORKMODULE_HPP
# define NETWORKMODULE_HPP

# include "IMonitorModule.hpp"

class NetworkModule : public IMonitorModule
{
private:
	std::map<std::string,std::string> _data;
public:
	NetworkModule();
	NetworkModule(NetworkModule const &src);
	~NetworkModule();

	NetworkModule &operator=(NetworkModule const &src);

	void									fillData();
	void 									dell_data();
	std::map<std::string, std::string>	*	get_data();
};

#endif
