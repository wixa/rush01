/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   DateTimeModule.hpp                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kshyshki <kshyshki@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/14 10:14:00 by kshyshki          #+#    #+#             */
/*   Updated: 2018/04/14 10:14:00 by kshyshki         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */


#ifndef DATETIMEMODULE_HPP
# define DATETIMEMODULE_HPP

# include "IMonitorModule.hpp"
# include <iomanip>
# include <ctime>
# include <sstream>
# include <iostream>
class DateTimeModule : public IMonitorModule
{
private:
	std::map<std::string,std::string> _data;
public:
	DateTimeModule();
	DateTimeModule(DateTimeModule const &src);
	~DateTimeModule();

	DateTimeModule &	operator=(DateTimeModule const &src);

	void									fillData();
	void 									dell_data();
	std::map<std::string, std::string> *	get_data();
};

#endif
