/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   IMonitorModule.hpp                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kshyshki <kshyshki@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/14 08:38:00 by kshyshki          #+#    #+#             */
/*   Updated: 2018/04/14 08:38:00 by kshyshki         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */


#ifndef IMONITORMODULE_HPP
# define IMONITORMODULE_HPP

# include <sys/types.h>
# include <sys/sysctl.h>
# include <unistd.h>
# include <sstream>
# include <iostream>
# include <map>

class IMonitorModule
{
public:
	virtual ~IMonitorModule(){};
	virtual void									fillData() = 0;
	virtual void									dell_data() = 0;
	virtual std::map<std::string, std::string>	*	get_data() = 0;
};

#endif
