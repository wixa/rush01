/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ADisplay.cpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kshyshki <kshyshki@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/14 15:32:00 by kshyshki          #+#    #+#             */
/*   Updated: 2018/04/14 15:32:00 by kshyshki         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ADisplay.hpp"

static void	free_module(IMonitorModule * module)
{
	delete module;
}

ADisplay::ADisplay()
{
	_modules.reserve(6);
	_modules.push_back(new HostUserModule());
	_modules.push_back(new OSInfoModule());
	_modules.push_back(new DateTimeModule());
	_modules.push_back(new CPUModule());
	_modules.push_back(new RamModule());
	_modules.push_back(new NetworkModule());
}

ADisplay::ADisplay(ADisplay const &src)
{
	*this = src;
}

ADisplay::~ADisplay()
{
	std::for_each(_modules.begin(), _modules.end(), &free_module);
}

ADisplay &	ADisplay::operator=(ADisplay const &src)
{
	if (this != &src)
	{
		_modules = src._modules;
	}
	return *this;
}

void 		ADisplay::fillAllData()
{
	for (int i = 0; i < 6; i++)
		_modules.at(i)->fillData();
}

std::vector<IMonitorModule*> ADisplay::getModules()
{
	return _modules;
}