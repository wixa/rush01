/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   RamModule.hpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kshyshki <kshyshki@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/14 11:34:00 by kshyshki          #+#    #+#             */
/*   Updated: 2018/04/14 11:34:00 by kshyshki         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */


#ifndef RAMMODULE_HPP
# define RAMMODULE_HPP

# include "IMonitorModule.hpp"

class RamModule : public IMonitorModule
{
private:
	std::map<std::string,std::string> _data;
public:
	RamModule();
	RamModule(RamModule const &src);
	~RamModule();

	RamModule &	operator=(RamModule const &src);

	void									fillData();
	void 									dell_data();
	std::map<std::string, std::string> *	get_data();
};

#endif
