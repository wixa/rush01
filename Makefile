# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: kshyshki <kshyshki@student.unit.ua>        +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/04/02 09:25:31 by kshyshki          #+#    #+#              #
#    Updated: 2018/04/14 17:00:01 by kshyshki         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

CLANG   = clang++
FLAGS	= 
NAME	= ft_gkrellm

INCPATH =	-I ~/Qt/5.4/clang_64/lib/QtWidgets.framework/Versions/5/Headers \
			-I ~/Qt/5.4/clang_64/lib/QtGui.framework/Versions/5/Headers \
			-I ~/Qt/5.4/clang_64/lib/QtCore.framework/Versions/5/Headers \
			-I. -F ~/Qt/5.4/clang_64/lib

LIBS          = -framework QtWidgets -framework QtGui -framework QtCore -lncurses

SRC = NcurseDisplay.cpp				\
      HostUserModule.cpp          	\
      OSInfoModule.cpp            	\
      DateTimeModule.cpp          	\
      CPUModule.cpp               	\
      RamModule.cpp               	\
      NetworkModule.cpp           	\
      Window.cpp                    \
      moc_window.cpp                \
      ADisplay.cpp                	\
      UIDisplay.cpp              	\
      ft_gkrellm.cpp				\

OBJ=$(SRC:.cpp=.o)

all: $(NAME)

$(NAME): $(OBJ)
	$(CLANG) $(FLAGS) $(INCPATH) -o $@ $^ $(LIBS)

%.o: %.cpp
	$(CLANG) $(FLAGS) $(INCPATH) -o $@ -c $<

clean:
	rm -rf $(OBJ)

fclean: clean
	rm -rf ft_gkrellm

re: fclean all
