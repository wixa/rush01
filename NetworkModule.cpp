/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   NetworkModule.cpp                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kshyshki <kshyshki@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/14 11:37:00 by kshyshki          #+#    #+#             */
/*   Updated: 2018/04/14 11:37:00 by kshyshki         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "NetworkModule.hpp"

void 	NetworkModule::dell_data()
{
	_data.clear();
}

NetworkModule::NetworkModule()
{
	this->fillData();
}

NetworkModule::NetworkModule(NetworkModule const &src)
{
	*this = src;
}

NetworkModule::~NetworkModule()
{
	this->dell_data();
}

NetworkModule &	NetworkModule::operator=(NetworkModule const &src)
{
	if (this != &src)
		_data = src._data;
	return *this;
}

void			NetworkModule::fillData()
{
	std::stringstream ss;
	char buff[64];
	FILE *ret = popen("top -l 1 | head -n 10 |grep  Networks", "r");
	while (!feof(ret))
	{
		if (fgets(buff, 64, ret))
			ss << buff;
	}
	pclose(ret);
	std::string tmp_str = ss.str();
	size_t start = tmp_str.rfind(':', std::string::npos);
	size_t end = tmp_str.find_first_of(' ' , start + 2);
	_data["1.Input packets/memory : "] = std::string(tmp_str.substr(start + 2, end - start - 2));
	start = tmp_str.find_last_of(',', std::string::npos);
	end = tmp_str.find_first_of(' ' , start + 2);
	_data["2.Output packets/memory : "] = std::string(tmp_str.substr(start + 2, end - start - 2));
}

std::map<std::string, std::string> *	NetworkModule::get_data()
{
	return &_data;
};