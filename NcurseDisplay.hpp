/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   NcurseDisplay.hpp                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kshyshki <kshyshki@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/14 11:41:00 by kshyshki          #+#    #+#             */
/*   Updated: 2018/04/14 11:41:00 by kshyshki         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */


#ifndef NCURSEDISPLAY_HPP
# define NCURSEDISPLAY_HPP

# include <iostream>
# include <vector>
# include <ncurses.h>
# include "ADisplay.hpp"

class NcurseDisplay : public ADisplay
{
public:
	NcurseDisplay();
	NcurseDisplay(NcurseDisplay const &src);
	~NcurseDisplay();

	NcurseDisplay &	operator=(NcurseDisplay const &src);

	void 			start();
	int				keyfunc(int key);
};

#endif
