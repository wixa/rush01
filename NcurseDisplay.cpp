/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   NcurseDisplay.cpp                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kshyshki <kshyshki@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/14 11:41:00 by kshyshki          #+#    #+#             */
/*   Updated: 2018/04/14 11:41:00 by kshyshki         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "NcurseDisplay.hpp"

NcurseDisplay::NcurseDisplay()
{
	std::cout << "\033[8,60;220t";
	initscr();
	cbreak();
	clear();
	mousemask(ALL_MOUSE_EVENTS | REPORT_MOUSE_POSITION, nullptr);
	start_color();
	init_pair(1, COLOR_WHITE, COLOR_BLACK);
	init_pair(2, COLOR_RED, COLOR_BLACK);
	init_pair(3, COLOR_YELLOW, COLOR_BLACK);
	init_pair(4, COLOR_GREEN, COLOR_BLACK);
	init_pair(5, COLOR_CYAN, COLOR_BLACK);
	init_pair(6, COLOR_MAGENTA, COLOR_BLACK);
	erase();
	keypad(stdscr,TRUE);
	nodelay(stdscr, TRUE);
	curs_set(0);
	noecho();
	refresh();
}

NcurseDisplay::NcurseDisplay(NcurseDisplay const &src)
{
	*this = src;
}

NcurseDisplay::~NcurseDisplay()
{

}

NcurseDisplay &NcurseDisplay::operator=(NcurseDisplay const &src)
{
	static_cast<void>(src);
	return *this;
}

static void 		display(std::pair<std::string, std::string> cell)
{
	std::string tmp = cell.first + cell.second;
	printw("%s\n", tmp.c_str());
}

void 		NcurseDisplay::start()
{
	std::string headers[6] =
			{
					"----------------------HOST/NAME INFO----------------------",
					"--------------------------OS INFO-------------------------",
					"----------------------DATE/TIME INFO----------------------",
					"-------------------------CPU INFO-------------------------",
					"-------------------------RAM INFO-------------------------",
					"-----------------------NETWORK INFO-----------------------"
			};

	int c = 0;
	while (1)
	{
		int y = 0;
		int x = 0;
		c = getch();
		if (!keyfunc(c))
			break ;
		this->fillAllData();
		for(int i = 0; i < 6; i++)
		{
			move(y += 1, x);
			printw(headers[i].c_str());
			move(y + 1, x);
			std::map<std::string, std::string> * data = _modules[i]->get_data();
			std::for_each(data->begin(), data->end(), &display);
			move(y += _modules[i]->get_data()->size(), x);
		}
	}
}

int				NcurseDisplay::keyfunc(int key)
{
	erase();
	if (key == 'q')
		return (0);
	return (1);
}