/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   DateTimeModule.cpp                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kshyshki <kshyshki@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/14 10:14:00 by kshyshki          #+#    #+#             */
/*   Updated: 2018/04/14 10:14:00 by kshyshki         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "DateTimeModule.hpp"

void 	DateTimeModule::dell_data()
{
	_data.clear();
}

DateTimeModule::DateTimeModule()
{
	this->fillData();
}

DateTimeModule::DateTimeModule(DateTimeModule const &src)
{
	*this = src;
}

DateTimeModule::~DateTimeModule()
{
	this->dell_data();
}

DateTimeModule &	DateTimeModule::operator=(DateTimeModule const &src)
{
	if (this != &src)
		_data = src._data;
	return *this;
}

void				DateTimeModule::fillData()
{
	this->dell_data();
	std::stringstream ss_date;
	std::stringstream ss_time;
	std::time_t rawtime = time(nullptr);
	struct tm *timeinfo = localtime (&rawtime);

	ss_date	<< (timeinfo->tm_year + 1900) << "\\"
		  	<< std::setw(2) << std::setfill('0') << (timeinfo->tm_mon + 1) << "\\"
		  	<< std::setw(2) << std::setfill('0') << timeinfo->tm_mday;
	ss_time << std::setw(2) << std::setfill('0') << timeinfo->tm_hour << ":"
			<< std::setw(2) << std::setfill('0') << timeinfo->tm_min << ":"
			<< std::setw(2) << std::setfill('0') << timeinfo->tm_sec;

	_data.insert(std::pair<std::string, std::string>("1.Date : ", std::string(ss_date.str())));
	_data.insert(std::pair<std::string, std::string>("2.Time : ", std::string(ss_time.str())));

}

std::map<std::string, std::string> * 	DateTimeModule::get_data()
{
	return &_data;
};