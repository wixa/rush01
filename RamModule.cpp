/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   RamModule.cpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kshyshki <kshyshki@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/14 11:34:00 by kshyshki          #+#    #+#             */
/*   Updated: 2018/04/14 11:34:00 by kshyshki         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "RamModule.hpp"

void 	RamModule::dell_data()
{
	_data.clear();
}

RamModule::RamModule()
{
	this->fillData();
}

RamModule::RamModule(RamModule const &src)
{
	*this = src;
}

RamModule::~RamModule()
{
	this->dell_data();
}

RamModule &RamModule::operator=(RamModule const &src)
{
	if (this != &src)
		_data = src._data;
	return *this;
}

void		RamModule::fillData()
{
	std::stringstream ss;
	char buff[128];
	FILE *ret = popen("top -l 1 | head -n 10 | grep PhysMem", "r");
	while (!feof(ret)) {
		if (fgets(buff, 128, ret))
			ss << buff;
	}
	pclose(ret);
	std::string tmp_str = ss.str();
	size_t start = tmp_str.rfind(':', std::string::npos);
	size_t end = tmp_str.find_first_of(' ' , start + 2);
	_data["1.Ram used : "] = std::string(tmp_str.substr(start + 2, end - start - 2));
	start = tmp_str.find_last_of(',', std::string::npos);
	end = tmp_str.find_first_of(' ' , start + 2);
	_data["2.Ram free : "] = std::string(tmp_str.substr(start + 2, end - start - 2));
}

std::map<std::string, std::string> *	RamModule::get_data()
{
	return &_data;
};