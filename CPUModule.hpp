/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   CPUModule.hpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kshyshki <kshyshki@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/14 10:18:00 by kshyshki          #+#    #+#             */
/*   Updated: 2018/04/14 10:18:00 by kshyshki         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */


#ifndef CPUMODULE_HPP
# define CPUMODULE_HPP

# include "IMonitorModule.hpp"

class CPUModule : public IMonitorModule
{
private:
	std::map<std::string,std::string> _data;
public:
	CPUModule();
	CPUModule(CPUModule const &src);
	~CPUModule();

	CPUModule &	operator=(CPUModule const &src);

	void									fillData();
	void 									dell_data();
	std::map<std::string, std::string> *	get_data();
};

#endif
