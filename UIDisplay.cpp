/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   UIDisplay.cpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kshyshki <kshyshki@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/14 11:39:00 by kshyshki          #+#    #+#             */
/*   Updated: 2018/04/14 11:39:00 by kshyshki         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "UIDisplay.hpp"
#include "Window.hpp"

UIDisplay::UIDisplay()
{

}

UIDisplay::UIDisplay(UIDisplay const &src)
{
	*this = src;
}

UIDisplay::~UIDisplay()
{

}

UIDisplay &	UIDisplay::operator=(UIDisplay const &src)
{
	static_cast<void>(src);
	return *this;
}

void 		UIDisplay::start()
{
	int argc = 0;
	char **argv = nullptr;
	QApplication app(argc, argv);
	this->fillAllData();
	Window window(_modules, 0);
	window.show();
	app.exec();
}

