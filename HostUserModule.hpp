/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   HostUserModule.hpp                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kshyshki <kshyshki@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/14 09:59:00 by kshyshki          #+#    #+#             */
/*   Updated: 2018/04/14 09:59:00 by kshyshki         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */


#ifndef HOSTUSERMODULE_HPP
# define HOSTUSERMODULE_HPP

# include "IMonitorModule.hpp"

class HostUserModule : public IMonitorModule
{
private:
	std::map<std::string,std::string> _data;
public:
	HostUserModule();
	HostUserModule(HostUserModule const &src);
	~HostUserModule();

	HostUserModule &	operator=(HostUserModule const &src);

	void									fillData();
	void									dell_data();
	std::map<std::string, std::string>	*	get_data();
};

#endif
