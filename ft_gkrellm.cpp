/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_gkrellm.cpp                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kshyshki <kshyshki@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/14 12:19:00 by kshyshki          #+#    #+#             */
/*   Updated: 2018/04/14 12:19:00 by kshyshki         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "NcurseDisplay.hpp"
#include "UIDisplay.hpp"
#include <iostream>

 int main(int argc, char **argv)
 {
	 if (argc != 2)
	 {
		 std::cout << "USAGE: ./ft_gkrellm [-g/-t]\n-t - text mode\n-g - graphical mode"
				   << std::endl;
		 return 1;
	 }
	 else
	 {
		 std::string flag(argv[1]);
		 if (!flag.compare("-t"))
		 {
			 NcurseDisplay display = NcurseDisplay();
			 display.start();
		 }
		 else if (!flag.compare("-g"))
		 {
			 UIDisplay display = UIDisplay();
			 display.start();
		 }
	 }
	 return 0;
 }

