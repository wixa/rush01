/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   UIDisplay.hpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kshyshki <kshyshki@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/14 11:39:00 by kshyshki          #+#    #+#             */
/*   Updated: 2018/04/14 11:39:00 by kshyshki         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */


#ifndef UIDISPLAY_HPP
# define UIDISPLAY_HPP

# include <iostream>
# include <vector>
# include "ADisplay.hpp"
# include <QtWidgets/QApplication>

class UIDisplay : public ADisplay
{
public:
	UIDisplay();
	UIDisplay(UIDisplay const &src);
	~UIDisplay();

	UIDisplay &	operator=(UIDisplay const &src);

	void 		start();

};

#endif
