/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   OSInfoModule.hpp                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kshyshki <kshyshki@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/14 10:05:00 by kshyshki          #+#    #+#             */
/*   Updated: 2018/04/14 10:05:00 by kshyshki         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */


#ifndef OSINFOMODULE_HPP
# define OSINFOMODULE_HPP

# include "IMonitorModule.hpp"

class OSInfoModule : public IMonitorModule
{
private:
	std::map<std::string,std::string> _data;
public:
	OSInfoModule();
	OSInfoModule(OSInfoModule const &src);
	~OSInfoModule();

	OSInfoModule &operator=(OSInfoModule const &src);

	void									fillData();
	void									dell_data();
	std::map<std::string, std::string> *	get_data();
};

#endif
